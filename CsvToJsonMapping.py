import pandas as pd
import numpy as np
import json

df = pd.read_csv(r"""/Users/dqin/Downloads/ES_demo_index20180801.csv""")

ES_index_name = "sample_index"
ES_type_name = "sample_type"


mappings_d = {}

# mappings_d[ES_index_name] = {"mappings":{ES_type_name:{"properties":{}}}}
mappings_d['settings'] = json.loads('''{
    "analysis": {
      "normalizer":{
        "lower_case":{
          "type":"custom",
          "filter":["lowercase"]
        }
      }
    },
    "number_of_shards": 5,
    "number_of_replicas": 2
  }''')


#Within the field "properties", you put in key-value pairs for field & ES data type mapping

for index, row in df.iterrows():
    # mappings_d[row["Elastic Search field name"]] = row["Elastic Search Data Type"]
    # print(row["Elastic Search field name"])
    # print(row["Elastic Search Data Type"])
    # mappings_d[ES_index_name]["mappings"][ES_type_name]["properties"][row["Elastic Search field name"]]={"type":row["Elastic Search Data Type"]}
    if ("mappings" not in mappings_d):
        mappings_d["mappings"] = {}
    if (ES_type_name not in mappings_d["mappings"]):
        mappings_d["mappings"][ES_type_name] = {}
    if ("properties" not in mappings_d["mappings"][ES_type_name]):
        mappings_d["mappings"][ES_type_name]["properties"] = {}
    mappings_d["mappings"][ES_type_name]["properties"][row["Elastic Search field name"]] = {"type":row["Elastic Search Data Type"]}
    if (row["Elastic Search Data Type"].lower() == "keyword"):
        mappings_d["mappings"][ES_type_name]["properties"][row["Elastic Search field name"]]["normalizer"] = "lower_case"

result = json.dumps(mappings_d)

print(result)