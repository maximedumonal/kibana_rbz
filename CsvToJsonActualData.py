### This file convert csv format of demo RBZ data into json file
### The generated json file can be directly inserted into ES via bulk insert commmand


import pandas as pd
import json
import numpy as np


# Wrap the downloaded sample data into a proper JSON so ES can read it in, based on the data type specified in EXCEL
### Convert YES into true
### Convert String into numbers, for some entry
### Skip all the nan

data = pd.read_csv("""/Users/dqin/Downloads/sample_data_1.csv""")
# Column name is the ES field name, first row(i.e. row 0) is the data type

# A dataframe specifically for field names and types
name_n_type = data.head(1)

# define constants
string_set = {"keyword", "text", "date"}
boolean_set = {"boolean"}
number_set = {"double", "long", "integer", "float"}

list_of_data = []
for index, row in data.iterrows():
    if index >= 1:
        print("now at row " + str(index))
        cur_d = {}
        for cur_field in list(name_n_type):
            cur_type = name_n_type.loc[0, cur_field].lower() # get the lower-case type

            # Skip if this field is empty (nan)
            if isinstance(row[cur_field], float) and np.isnan(row[cur_field]):
                continue

            if cur_type in string_set: # Keep all the text & keyword
                cur_d[cur_field] = row[cur_field]
            elif cur_type in number_set:
                if cur_type == "double" or cur_type == "float":
                    cur_d[cur_field] = float(row[cur_field])
                else:
                    cur_d[cur_field] = int(row[cur_field])
            elif cur_type in boolean_set: # Convert "YES" to True
                if row[cur_field].lower() == "yes":
                    cur_d[cur_field] = True
                else:
                    cur_d[cur_field] = False
            else:
                print("There is an TYPE that cannot be mapped... " + cur_type + " at row " + str(index))
                break;
        list_of_data.append(cur_d)



temp3 = json.loads('{"index":{"_index":"sample_index","_type":"sample_type"}}')
# index_no = 0
with open("""/Users/dqin/Downloads/data_to_insert_ES.json""", "w") as f:
    for cur_dict in list_of_data:
        print(json.dumps(cur_dict))
#     then write to the desired json file
        f.write(json.dumps(temp3))
        f.write("\n")
        f.write(json.dumps(cur_dict))
        f.write("\n")


# TO bulk-insert data into ES, run the following command in terminal
# curl -s -H "Content-Type: application/x-ndjson" -XPOST localhost:9200/_bulk --data-binary "@/Users/dqin/Downloads/data_to_insert_ES.json"

